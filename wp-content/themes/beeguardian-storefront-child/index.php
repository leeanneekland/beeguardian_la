<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */?>


<?php get_header(); ?>
   
    <div id="hero_container">
        <img src="http://localhost/beeguardian_wp/wp-content/uploads/2017/07/beeguardian-frontpage-image-3.jpg">
            <div class="vertical-center">
            <h2>Let's let bees be bees</h2>
            <a href="/product/bee-guardian-honey/" class="btn">Shop Now</a>
        </div>
    </div> #primary 
    
<div class="product-highlights">
	<div class="highlight-section">
       <div class="highlight-image" id="spoon">
			<img src="http://localhost/beeguardian_wp/wp-content/uploads/2017/07/spoon.jpg">
		</div>
        <div class="text-container">
			<div class="highlight-text" id="flavor">
				<h1>UNCOMPROMISING FLAVOR</h1>
				<p>Great plant-based food doesn’t have to be complicated. In fact, it shouldn’t be. Like many things in life and nature, our pursuit of a rich, honey flavor crafted in the kitchen was not achieved by manufactured complexities and artificial processing, but from the exacting balance of simple ingredients by a passionate chef.</p>
			</div>
		</div>	
    </div>

	<div class="highlight-section">
       <div class="highlight-image">
			<img src="http://localhost/beeguardian_wp/wp-content/uploads/2017/07/ingredients.jpg">
		    </div>
        <div class="text-container">
            <div class="highlight-text" >
                <h1>ORGANIC INGREDIENTS</h1>
                <p>Eating organic is the simplest way to feed your body right. Our ingredients are always organic, sustainably harvested and of the highest quality.</p>
            </div> 
        </div>
    </div>
	<div class="highlight-section" >
        <div class="highlight-image">
            <img src="http://localhost/beeguardian_wp/wp-content/uploads/2017/07/labels.jpg">
        </div>
		<div class="text-container">
            <div class="highlight-text" id="labels">
                <h1>EARTH FRIENDLY</h1>
                <p>Being a truly earth friendly business means we are committed to helping you reduce your enviornmental footprint, which is why we’ve gone the extra mile to make our labels from compostable sugarcane based paper stock and printed with plant-based dyes and adhesives. Recycling your clean, used Bee Guardian keeps 100% of the waste out of landfills.</p>
            </div>
        </div>
	</div>	
</div>	


<?php get_footer();?>