
<!--    Hi Kevin. I'm not sure what this page is for. This code is part of index.php. Do you need to keep it?-->

    
    <div class="product-highlights">

	<div class="highlight-section" id="spoon">

		<div class="highlight-image">
			<img src="http://localhost:8888/wp-content/uploads/2017/07/spoon.jpg">
		</div>	
		<div class="text-container">
			<div class="highlight-text">
				<h1>UNCOMPROMISING FLAVOR</h1>
				<p>Great plant-based food doesn’t have to be complicated. In fact, it shouldn’t be. Like many things in life and nature, our pursuit of a rich, honey flavor crafted in the kitchen was not achieved by manufactured complexities and artificial processing, but from the exacting blanace of simple ingrediants by a passionate chef.</p>
			</div>
		</div>
	</div>
	<div class="highlight-section" id="ingredients">

		<div class="highlight-text">
			<h1>ORGANIC INGREDIENTS</h1>
			<p>Eating organic is the simplest way to feed your body right. Our ingredients are always organic, sustainably harvested and of the highest quality.</p>
		</div>
		<div class="highlight-image">
			<img src="http://localhost:8888/wp-content/uploads/2017/07/ingredients.jpg">
		</div>

	</div>
	<div class="highlight-section" id="labels">
		
		<div class="highlight-image">
			<img src="http://localhost:8888/wp-content/uploads/2017/07/labels.jpg">
		</div>
		<div class="highlight-text">
			<h1>EARTH FRIENDLY</h1>
			<p>Being a truly earth friendly business means we are committed to helping you reduce your enviornmental footprint, which is why we’ve gone the extra mile to make our labels from compostable sugarcane based paper stock and printed with plant-based dyes and adhesives. Recycling your clean, used Bee Guardian keeps 100% of the waste out of landfills.</p>
		</div>

	</div>

</div>