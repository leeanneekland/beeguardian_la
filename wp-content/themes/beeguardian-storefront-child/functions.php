<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
//function sf_child_theme_dequeue_style() {
//wp_dequeue_style( 'storefront-style' );
//wp_dequeue_style( 'storefront-woocommerce-style' );
//}

add_filter( 'woocommerce_get_breadcrumb', '__return_false' );



add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
  //  unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab


    return $tabs;

}
function remove_sf_actions() {

	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	remove_action( 'storefront_footer', 'storefront_handheld_footer_bar', 999 );	
    remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );

}
add_action( 'init', 'remove_sf_actions' );



/** REORDER SINGLE PRODUCT PAGE **/


function reorganize_single_product(){

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15) ;
add_action( 'init', 'reorganize_single_product' );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

 
function custom_override_checkout_fields( $fields ) {
    
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_phone']);
    unset($fields['billing']['billing_company']);
    return $fields;
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section id="main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

